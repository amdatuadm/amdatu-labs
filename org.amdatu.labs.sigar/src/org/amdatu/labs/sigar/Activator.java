/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.labs.sigar;

import java.util.Dictionary;
import java.util.Hashtable;

import org.apache.felix.service.command.CommandProcessor;
import org.hyperic.sigar.Mem;
import org.hyperic.sigar.Sigar;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {

    private Sigar m_sigar = new Sigar();

    @Override
    public void start(BundleContext context) throws Exception {

        Dictionary<String, Object> dict = new Hashtable<String, Object>();
        dict.put(CommandProcessor.COMMAND_SCOPE, "sigar");
        dict.put(CommandProcessor.COMMAND_FUNCTION, new String[] { "report" });
        context.registerService(Object.class.getName(), this, dict);

        report();
    }

    @Override
    public void stop(BundleContext context) throws Exception {
    }

    public void report() throws Exception {
        Mem mem = m_sigar.getMem();

        StringBuilder builder = new StringBuilder();
        builder.append("Memory: ").append(toMega(mem.getUsed())).append(" of ").append(toMega(mem.getTotal()))
                .append("mb used (").append((int) mem.getUsedPercent()).append("%)");
        System.out.println(builder.toString());
    }

    long toMega(long bytes) {
        return bytes / (1000000000);
    }
}
