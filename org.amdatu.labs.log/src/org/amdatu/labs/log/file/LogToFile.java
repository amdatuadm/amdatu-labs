package org.amdatu.labs.log.file;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedList;

import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogEntry;
import org.osgi.service.log.LogListener;
import org.osgi.service.log.LogReaderService;

public class LogToFile implements LogListener {
    private static final int LOG_BUFFER_SIZE = 4096;
    private volatile LogReaderService m_reader;
    private BufferedWriter m_writer;
    
    public void start() {
        try {
            String logFileName = createLogFileName();
            m_writer = new BufferedWriter(new FileWriter(new File(logFileName)), LOG_BUFFER_SIZE);
            // since the history of the log has the most recent entry first, and we want that last,
            // we have to reverse the history using a linked list (adding entries at the front)
            // before writing everything to file
            LinkedList<String> lines = new LinkedList<String>();
            @SuppressWarnings("rawtypes")
            Enumeration logLines = m_reader.getLog();
            while (logLines.hasMoreElements()) {
                LogEntry entry = (LogEntry) logLines.nextElement();
                lines.addFirst(createLogLine(entry));
            }
            for (String line : lines) {
                log(line);
            }
            m_reader.addLogListener(this);
        }
        catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }
    
    public void stop() {
        m_reader.removeLogListener(this);
        try {
            m_writer.close();
        }
        catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }

    @Override
    public void logged(LogEntry entry) {
        log(createLogLine(entry));
    }

    /** Write a log line to disk, append a platform dependent newline. */
    void log(String line) {
        try {
            m_writer.write(line);
            m_writer.newLine();
        }
        catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }

    /** Create a log line string, based on a log entry. */
    String createLogLine(LogEntry entry) {
        String trace = "";
        Throwable t = entry.getException();
        if (t != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(baos);
            t.printStackTrace(ps);
            trace = baos.toString();
        }
        String service = "-";
        ServiceReference ref = entry.getServiceReference();
        if (ref != null) {
            String[] api = (String[]) ref.getProperty(Constants.OBJECTCLASS);
            if (api != null) {
                StringBuffer apis = new StringBuffer();
                apis.append("[");
                for (String s : api) {
                    if (apis.length() > 1) {
                        apis.append(", ");
                    }
                    apis.append(s);
                }
                apis.append("]");
                service = apis.toString();
            }
        }
        return String.format("%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS.%1$tL [%2$d] [%3$d] %4$s %5$s %6$s",
            new Date(entry.getTime()), 
            entry.getLevel(), 
            entry.getBundle().getBundleId(), 
            service,
            entry.getMessage(),
            trace
        );
    }
    
    /** Create a log file name, based on the current date and time. */
    String createLogFileName() {
        return String.format("log-%1$tY-%1$tm-%1$td_%1$tH-%1$tM-%1$tS_%1$tL.txt", new Date());
    }
}
