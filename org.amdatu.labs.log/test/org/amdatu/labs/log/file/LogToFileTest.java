package org.amdatu.labs.log.file;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Map;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.Version;
import org.osgi.service.log.LogEntry;
import org.osgi.service.log.LogService;

public class LogToFileTest extends TestCase {
	public void testLogFileNameCreator() throws Exception {
		LogToFile instance = new LogToFile();
		String name = instance.createLogFileName();
		Thread.sleep(1);
		String name2 = instance.createLogFileName();
		Assert.assertNotSame("Log file names should be different", name, name2);
	}

	public void testCreateLogLine() throws Exception {
		LogToFile instance = new LogToFile();
		String line = instance.createLogLine(new LogEntry() {
			@Override
			public Bundle getBundle() {
				return new Bundle() {

					@Override
					public int getState() {
						return 0;
					}

					@Override
					public void start(int options) throws BundleException {
					}

					@Override
					public void start() throws BundleException {
					}

					@Override
					public void stop(int options) throws BundleException {
					}

					@Override
					public void stop() throws BundleException {
					}

					@Override
					public void update(InputStream input)
							throws BundleException {
					}

					@Override
					public void update() throws BundleException {
					}

					@Override
					public void uninstall() throws BundleException {
					}

					@Override
					public Dictionary getHeaders() {
						return null;
					}

					@Override
					public long getBundleId() {
						return 66;
					}

					@Override
					public String getLocation() {
						return null;
					}

					@Override
					public ServiceReference[] getRegisteredServices() {
						return null;
					}

					@Override
					public ServiceReference[] getServicesInUse() {
						return null;
					}

					@Override
					public boolean hasPermission(Object permission) {
						return false;
					}

					@Override
					public URL getResource(String name) {
						return null;
					}

					@Override
					public Dictionary getHeaders(String locale) {
						return null;
					}

					@Override
					public String getSymbolicName() {
						return null;
					}

					@Override
					public Class loadClass(String name)
							throws ClassNotFoundException {
						return null;
					}

					@Override
					public Enumeration getResources(String name)
							throws IOException {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public Enumeration getEntryPaths(String path) {
						return null;
					}

					@Override
					public URL getEntry(String path) {
						return null;
					}

					@Override
					public long getLastModified() {
						return 0;
					}

					@Override
					public Enumeration findEntries(String path,
							String filePattern, boolean recurse) {
						return null;
					}

					@Override
					public BundleContext getBundleContext() {
						return null;
					}

					@Override
					public Map getSignerCertificates(int signersType) {
						return null;
					}

					@Override
					public Version getVersion() {
						return null;
					}

					@Override
					public int compareTo(Bundle arg0) {
						// TODO Auto-generated method stub
						return 0;
					}

					@Override
					public <A> A adapt(Class<A> type) {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public File getDataFile(String filename) {
						// TODO Auto-generated method stub
						return null;
					}
				};
			}

			@Override
			public ServiceReference getServiceReference() {
				return null;
			}

			@Override
			public int getLevel() {
				return LogService.LOG_INFO;
			}

			@Override
			public String getMessage() {
				return "log message";
			}

			@Override
			public Throwable getException() {
				return new Throwable("exception", new Throwable("cause"));
			}

			@Override
			public long getTime() {
				return 1;
			}
		});
		Assert.assertNotNull(line);
	}
}
