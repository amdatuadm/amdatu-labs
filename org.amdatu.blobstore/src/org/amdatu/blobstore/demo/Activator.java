/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore.demo;

import java.util.Dictionary;
import java.util.Hashtable;

import org.amdatu.blobstore.BlobStoreListener;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.osgi.service.log.LogService;

/**
 * The OSGi bundle activator for Amdatu BlobStore demo. This activator registers
 * a configurator component that sets up the demo environment.
 * 
 */
public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {

        manager.add(createComponent().setImplementation(DemoConfigurator.class)
                .add(createServiceDependency().setService(ConfigurationAdmin.class).setRequired(true))
                .add(createServiceDependency().setService(LogService.class).setRequired(false)));

        manager.add(createComponent().setInterface(BlobStoreListener.class.getName(), null)
                .setImplementation(DemoBlobStoreListener.class)
                .add(createServiceDependency().setService(ConfigurationAdmin.class).setRequired(true)));

        Dictionary<String, Object> properties = new Hashtable<String, Object>();
        properties.put(EventConstants.EVENT_TOPIC, new String[] { "org/amdatu/blobstore/*" });
        manager.add(createComponent().setInterface(EventHandler.class.getName(), properties).setImplementation(
                DemoBlobStoreEventHandler.class));
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {

    }

}
