/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore.demo;

import java.io.IOException;
import java.util.Dictionary;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

import org.amdatu.blobstore.core.Constants;
import org.amdatu.storage.blobstore.BlobStoreService;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

/**
 * This component sets up the demo environment through configuration.
 * 
 */
public class DemoConfigurator {

    private final static String HTTP_PID = "org.apache.felix.http";
    private final static String JCLOUDSSTORAGE_PID = "org.amdatu.storage.servicefactory";

    private final Set<Configuration> m_configurations = new HashSet<Configuration>();
    private volatile ConfigurationAdmin m_configurationAdmin;

    public void start() throws Exception {
        
        System.out.println("DEMO: Configuring HTTP on port 8080");
        configureHttp(8080);
        
        System.out.println("DEMO: Configuring a transient JClouds service 'store1'");
        configureJCloudsStorage("store1");
        
        System.out.println("DEMO: Configuring a BlobStore for JClouds service 'store1' using container 'default' at path 'store1path'");
        configureJCloudsBlobStore("store1", "default", "store1path");
        
        System.out.println("DEMO: Configuring a BlobStore for InMeM service 'store2' at path 'store2path");
        configureInMemBlobStore("store2", "store2path");
        
        System.out.println("DEMO: Configuring a BlobStore for InMeM service 'store3' without a REST path");
        configureInMemBlobStore("store3", "");
        
        System.out.println("DEMO: Did you see all those nice callbacks and events? Cool stuff aye?");
        System.out.println("DEMO: Now go and play with the REST API at http://localhost:8080/blobstore");
        System.out.println("DEMO: or..... with Swagger at http://localhost:8080/docs");
        System.out.println("DEMO: or..... with Swagger UI at http://localhost:8080/ui/index.html");
        System.out.println("DEMO: or..... with Demo UI at http://localhost:8080/blobstore-ui/index.html");
    }

    public void stop() throws Exception {
        System.out.println("DEMO: Shutting down....");
        for (Configuration conf : m_configurations) {
            conf.delete();
        }
    }

    public void configureHttp(int port) throws IOException {
        Dictionary<String, Object> props = new Hashtable<String, Object>();
        props.put("org.osgi.service.http.enable", true);
        props.put("org.osgi.service.http.port", port);
        props.put("org.osgi.service.https.enable", false);
        Configuration conf = m_configurationAdmin.getConfiguration(HTTP_PID, null);
        m_configurations.add(conf);
        conf.update(props);
    }

    public void configureJCloudsBlobStore(String id, String container, String path) throws IOException {

        Dictionary<String, Object> props = new Hashtable<String, Object>();
        props.put(Constants.BLOBSTORE_STOREID, id);
        props.put(Constants.BLOBSTORE_DESCRIPTION, "A demo store on JClouds");
        props.put(Constants.BLOBSTORE_RESTPATH, path);
        props.put(org.amdatu.blobstore.jclouds.Constants.JCLOUDS_BLOBSTORE_CONTAINER, container);

        Configuration conf = m_configurationAdmin.createFactoryConfiguration(
                org.amdatu.blobstore.jclouds.Constants.JCLOUDS_FACTORY_PID, null);
        m_configurations.add(conf);
        conf.update(props);
    }

    public void configureInMemBlobStore(String id, String path) throws IOException {

        Dictionary<String, Object> props = new Hashtable<String, Object>();
        props.put(Constants.BLOBSTORE_STOREID, id);
        props.put(Constants.BLOBSTORE_DESCRIPTION, "A demo store on Memory");
        props.put(Constants.BLOBSTORE_RESTPATH, path);

        Configuration conf = m_configurationAdmin.createFactoryConfiguration(
                org.amdatu.blobstore.inmem.Constants.INMEM_FACTORY_PID, null);
        m_configurations.add(conf);
        conf.update(props);
    }

    public void configureJCloudsStorage(String id) throws IOException {
        Dictionary<String, Object> props = new Hashtable<String, Object>();
        props.put("id", id);
        props.put("provider", BlobStoreService.Providers.TRANSIENT.toString());
        props.put("identity", "user");
        props.put("secret", "password");
        Configuration conf = m_configurationAdmin.createFactoryConfiguration(JCLOUDSSTORAGE_PID, null);
        m_configurations.add(conf);
        conf.update(props);
    }
}
