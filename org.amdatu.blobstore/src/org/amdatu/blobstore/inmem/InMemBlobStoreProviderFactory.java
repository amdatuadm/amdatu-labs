/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore.inmem;

import static org.amdatu.blobstore.inmem.Constants.INMEM_FACTORY_PID;
import static org.amdatu.blobstore.inmem.Constants.INMEM_PROVIDER_TYPE;

import java.util.Dictionary;

import org.amdatu.blobstore.core.Constants;
import org.amdatu.blobstore.spi.BlobStoreProvider;
import org.amdatu.blobstore.spi.BlobStoreProviderFactoryBase;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.service.log.LogService;

/**
 * Managed service factory for {@link InMemBlobStoreProvider} using PID
 * {@link InMemBlobStoreProviderFactory#FACTORY_PID}.
 */
public class InMemBlobStoreProviderFactory extends BlobStoreProviderFactoryBase {

    @Override
    public String getName() {
        return INMEM_PROVIDER_TYPE;
    }

    @Override
    protected String getFactoryPID() {
        return INMEM_FACTORY_PID;
    }

    @Override
    protected String getProviderType() {
        return INMEM_PROVIDER_TYPE;
    }

    @Override
    protected Component createComponent(DependencyManager manager, Dictionary<String, ?> configuration,
            Dictionary<String, Object> properties) {

        String identifier = (String) configuration.get(Constants.BLOBSTORE_STOREID);
        String description = (String) configuration.get(Constants.BLOBSTORE_DESCRIPTION);

        Component component = manager.createComponent().setInterface(BlobStoreProvider.class.getName(), properties)
                .setImplementation(new InMemBlobStoreProvider(identifier, INMEM_PROVIDER_TYPE, description))
                .add(manager.createServiceDependency().setService(LogService.class).setRequired(false));

        return component;
    }
}
