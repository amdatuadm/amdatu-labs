/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore.inmem;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

//TODO thread safety
public class InMemBlob {

    private final String m_uuid;
    private final byte[] m_bytes;
    private Map<String, String> m_headers;

    public InMemBlob(String uuid, byte[] bytes) {

        if (uuid == null || uuid.equals(""))
            throw new IllegalArgumentException("InMemBlob constructiore requires a uuid");

        m_uuid = uuid;
        m_bytes = bytes;
    }

    public String getUUID() {
        return m_uuid;
    }

    public byte[] getBytes() {
        if (m_bytes == null)
            return new byte[0];
        byte[] bytes = new byte[m_bytes.length];
        System.arraycopy(m_bytes, 0, bytes, 0, m_bytes.length);
        return bytes;
    }

    public Map<String, String> getHeaders() {
        if (m_headers == null) {
            return new HashMap<String, String>();
        }
        return new HashMap<String, String>(m_headers);
    }

    void setHeaders(Map<String, String> headers) {
        m_headers = headers;
    }

    public InputStream getInput() {
        return new ByteArrayInputStream(m_bytes);
    }
}
