/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore.inmem;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.amdatu.blobstore.NoSuchBlobException;
import org.amdatu.blobstore.spi.BlobStoreProvider;
import org.osgi.service.log.LogService;

/**
 * Amdatu BlobStore provider implementation using heap storage.
 * 
 */
public class InMemBlobStoreProvider implements BlobStoreProvider {

    private final Map<String, InMemBlob> m_blobs = new HashMap<String, InMemBlob>();
    private final String m_storeID;
    private final String m_type;
    private final String m_description;

    private volatile LogService m_logService;

    public InMemBlobStoreProvider(String storeID, String type, String description) {
        m_storeID = storeID;
        m_type = type;
        m_description = description;
    }

    @Override
    public String getStoreID() {
        return m_storeID;
    }

    @Override
    public String getProviderType() {
        return m_type;
    }

    @Override
    public String getDescription() {
        return m_description;
    }

    @Override
    public List<String> listBlobUUIDs() {
        synchronized (m_blobs) {
            return new ArrayList<String>(m_blobs.keySet());
        }
    }

    @Override
    public Map<String, String> addBlob(String blobUUID, InputStream dataInput) {

        ByteArrayOutputStream baos = null;
        InMemBlob blob = null;
        try {
            baos = new ByteArrayOutputStream(1024);
            int b;
            while ((b = dataInput.read()) != -1) {
                baos.write(b);
            }
            blob = new InMemBlob(blobUUID, baos.toByteArray());
        } catch (IOException e) {
            return null;
        } finally {
            try {
                dataInput.close();
                if (baos != null)
                    baos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        synchronized (m_blobs) {
            m_blobs.put(blobUUID, blob);
        }
        return blob.getHeaders();
    }

    private InMemBlob getBlob(String blobUUID) throws NoSuchBlobException {
        InMemBlob blob = null;
        synchronized (m_blobs) {
            blob = m_blobs.get(blobUUID);
        }
        if (blob == null) {
            throw new NoSuchBlobException(blobUUID);
        }
        return blob;
    }

    @Override
    public InputStream getBlobInput(String blobUUID) throws NoSuchBlobException {
        InMemBlob blob = getBlob(blobUUID);
        return blob.getInput();
    }

    @Override
    public Map<String, String> getBlobHeaders(String blobUUID) throws NoSuchBlobException {
        InMemBlob blob = getBlob(blobUUID);
        return blob.getHeaders();
    }

    @Override
    public Map<String, String> setBlobHeaders(String blobUUID, Map<String, String> headers) throws NoSuchBlobException {
        InMemBlob blob = getBlob(blobUUID);
        blob.setHeaders(headers);
        return blob.getHeaders();
    }

    @Override
    public void remove(String blobUUID) throws NoSuchBlobException {
        synchronized (m_blobs) {
            m_blobs.remove(blobUUID);
        }
    }
}
