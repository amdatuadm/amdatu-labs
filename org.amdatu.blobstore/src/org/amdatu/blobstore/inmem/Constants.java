package org.amdatu.blobstore.inmem;


/**
 * Implementation constants for Amdatu BlobStore InMem provider.
 */
public interface Constants {

    /**
     * Factory PID for the InMem blobStore provider factory.
     */
    String INMEM_FACTORY_PID = "org.amdatu.blobstore.inmem.factory";

    /**
     * Provider type for the JClouds blobStore provider.
     */
    String INMEM_PROVIDER_TYPE = "JClouds";
}
