/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore.core;

/**
 * Implementation constants for Amdatu BlobStore.
 */
public interface Constants {

    /**
     * Configuration and ServiceRegistration property for the unique store
     * identifier.
     */
    String BLOBSTORE_STOREID = "org.amdatu.blobstore.id";

    /**
     * Configuration and ServiceRegistration property for the store description.
     */
    String BLOBSTORE_DESCRIPTION = "org.amdatu.blobstore.description";

    /**
     * Configuration and ServiceRegistration property for the store rest path.
     */
    String BLOBSTORE_RESTPATH = "org.amdatu.blobstore.rest.path";

    /**
     * ServiceRegistration property for the store provider type. The value is
     * determined by the provider implementation.
     */
    String BLOBSTORE_PROVIDERTYPE = "org.amdatu.blobstore.type";
}
