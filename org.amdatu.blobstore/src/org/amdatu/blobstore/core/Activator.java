/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore.core;

import java.util.Properties;

import org.amdatu.blobstore.BlobStore;
import org.amdatu.blobstore.BlobStoreListener;
import org.amdatu.blobstore.spi.BlobStoreProvider;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

/**
 * The OSGi bundle activator for Amdatu BlobStore core. This activator registers
 * a {@link BlobStore} service for every {@link BlobStoreProvider} in the
 * service registry.
 * 
 */
public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {

        Properties props = new Properties();
        manager.add(createAdapterService(BlobStoreProvider.class, null)
                .setInterface(BlobStore.class.getName(), props)
                .setImplementation(BlobStoreImpl.class)
                .add(createServiceDependency().setService(BlobStoreListener.class).setRequired(false)
                        .setCallbacks("addBlobStoreListener", "removeBlobStoreListener"))
                .add(createServiceDependency().setService(LogService.class).setRequired(false)));
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
    }
}
