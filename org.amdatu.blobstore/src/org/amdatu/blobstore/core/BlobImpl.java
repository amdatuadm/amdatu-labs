/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore.core;

import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.amdatu.blobstore.Blob;
import org.amdatu.blobstore.BlobHeader;
import org.amdatu.blobstore.BlobStore;
import org.amdatu.blobstore.BlobStoreException;
import org.amdatu.blobstore.NoSuchBlobException;

/**
 * Internal thread-safe implementation of the blob interface
 */
public class BlobImpl implements Blob {

    private final String m_uuid;
    private final BlobStoreImpl m_blobStore;
    private final AtomicBoolean m_removed = new AtomicBoolean(false);
    private volatile Map<String, String> m_headers;

    BlobImpl(String blobUUID, BlobStoreImpl blobStore, Map<String, String> headers) {
        m_uuid = blobUUID;
        m_blobStore = blobStore;
        m_headers = headers;
    }

    @Override
    public BlobStore getStore() {
        return m_blobStore;
    }

    @Override
    public String getUUID() {
        return m_uuid;
    }

    @Override
    public Date getCreated() throws BlobStoreException {
        state();
        return internalGetDateHeader(BlobHeader.CREATED.headerKey());
    }

    @Override
    public Date getLastAccessed() throws BlobStoreException {
        state();
        return internalGetDateHeader(BlobHeader.LASTACCESSED.headerKey());
    }

    @Override
    public Date getLastModified() throws BlobStoreException {
        state();
        return internalGetDateHeader(BlobHeader.LASTMODIFIED.headerKey());
    }

    @Override
    public long getContentSize() throws BlobStoreException {
        state();
        return internalGetLongHeader(BlobHeader.CONTENTSIZE.headerKey());
    }

    @Override
    public String getContentType() throws BlobStoreException {
        state();
        return internalGetHeader(BlobHeader.CONTENTTYPE.headerKey());
    }

    @Override
    public String getContentMD5() throws BlobStoreException {
        state();
        return internalGetHeader(BlobHeader.CONTENTMD5.headerKey());
    }

    @Override
    public InputStream getContent() throws BlobStoreException {
        state();
        return m_blobStore.internalGetBlobInput(internalGetUUID());
    }

    @Override
    public Map<String, String> getHeaders() throws BlobStoreException {
        state();
        return new HashMap<String, String>(internalGetHeaders());
    }

    @Override
    public void updateHeaders(Map<String, String> update, boolean retainExisting) throws BlobStoreException {
        state();
        m_blobStore.internalUpdateBlobHeaders(this, update, retainExisting);
    }

    @Override
    public void remove() throws BlobStoreException {
        state();
        if (m_removed.compareAndSet(false, true)) {
            m_blobStore.internalRemoveBlob(this);
        }
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && (obj instanceof BlobImpl) && ((BlobImpl) obj).internalGetUUID().equals(internalGetUUID());
    }

    @Override
    public int hashCode() {
        return internalGetUUID().hashCode();
    }

    @Override
    public String toString() {
        return "blob " + internalGetUUID();
    }

    /**
     * Checks whether this blob has been removed.
     * 
     * @throws NoSuchBlobException
     *             If the blob was removed
     */
    void state() throws NoSuchBlobException {
        if (m_removed.get())
            throw new NoSuchBlobException("Blob removed " + m_uuid);
    }

    /**
     * Return the UUID without state check.
     * 
     * @return The uuid
     */
    String internalGetUUID() {
        return m_uuid;
    }

    /**
     * Initializes and returns the headers without state check or defensive
     * copy.
     * 
     * @return The headers
     * @throws NoSuchBlobException
     */
    Map<String, String> internalGetHeaders() throws BlobStoreException {
        if (m_headers == null) {
            try {
                m_headers = m_blobStore.internalGetBlobHeaders(internalGetUUID());
            } catch (NoSuchBlobException e) {
                m_removed.set(true);
                throw e;
            }
        }
        if (m_headers == null)
            m_headers = new HashMap<String, String>();
        return m_headers;
    }

    /**
     * Sets the headers for this blob without any integrity checks.
     * 
     * @param headers
     *            The headers
     */
    void internalSetHeaders(Map<String, String> headers) {
        m_headers = headers;
    }

    /**
     * Return the string value of a header without a state check.
     * 
     * @param key
     *            The header key
     * @return The header value
     * @throws NoSuchBlobException
     *             If headers can not be retrieved from the store
     */
    String internalGetHeader(String key) throws BlobStoreException {
        String header = internalGetHeaders().get(key);
        if (header == null)
            throw new IllegalStateException("Failed to retrieve header " + key + "for blob " + internalGetUUID());
        return header;

    }

    /**
     * Return the date value of a header without a state check.
     * 
     * @param key
     *            The header key
     * @return The header value
     * @throws NoSuchBlobException
     *             If headers can not be retrieved from the store
     */
    long internalGetLongHeader(String key) throws BlobStoreException {
        String header = internalGetHeader(key);
        try {
            return Long.parseLong(header);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to parse long header " + key + "for blob " + internalGetUUID(), e);
        }
    }

    /**
     * Return the date value of a header without a state check.
     * 
     * @param key
     *            The header key
     * @return The header value
     * @throws NoSuchBlobException
     *             If headers can not be retrieved from the store
     */
    Date internalGetDateHeader(String key) throws BlobStoreException {
        String header = internalGetHeader(key);
        try {
            return new Date(Long.parseLong(header));
        } catch (Exception e) {
            throw new IllegalStateException("Failed to parse date header " + key + "for blob " + internalGetUUID(), e);
        }
    }
}
