/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore.core;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

import org.amdatu.blobstore.Blob;
import org.amdatu.blobstore.BlobHeader;
import org.amdatu.blobstore.BlobStore;
import org.amdatu.blobstore.BlobStoreException;
import org.amdatu.blobstore.BlobStoreListener;
import org.amdatu.blobstore.InvalidFilterException;
import org.amdatu.blobstore.NoSuchBlobException;
import org.amdatu.blobstore.spi.BlobStoreProvider;
import org.amdatu.blobstore.util.ConcurrentWeakCache;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.log.LogService;

import com.google.common.io.CountingInputStream;

/**
 * Internal {@link BlobStore} implementation that wraps a
 * {@link BlobStoreProvider} providing find capabilities and a layer of
 * indirection.
 * 
 */
public class BlobStoreImpl implements BlobStore {

    private final ConcurrentWeakCache<String, BlobImpl> m_blobCache = new ConcurrentWeakCache<String, BlobImpl>();
    private final CopyOnWriteArrayList<BlobStoreListener> m_listeners = new CopyOnWriteArrayList<BlobStoreListener>();

    private volatile BlobStoreProvider m_blobStoreProvider;
    private volatile LogService m_logService;

    public BlobStoreImpl() {
    }

    BlobStoreImpl(BlobStoreProvider provider) {
        m_blobStoreProvider = provider;
    }

    void addBlobStoreListener(BlobStoreListener listener) {
        m_listeners.add(listener);
        try {
            listener.registered(this);
        } catch (Exception e) {
            m_logService.log(LogService.LOG_WARNING, "BlobStoreListener threw exception", e);
        }
    }

    void removeBlobStoreListener(BlobStoreListener listener) {
        m_listeners.remove(listener);
        try {
            listener.unregistered(this);
        } catch (Exception e) {
            m_logService.log(LogService.LOG_WARNING, "BlobStoreListener threw exception", e);
        }
    }

    @Override
    public String getID() {
        return m_blobStoreProvider.getStoreID();
    }

    @Override
    public String getProviderType() {
        return m_blobStoreProvider.getProviderType();
    }

    @Override
    public String getDescription() {
        return m_blobStoreProvider.getDescription();
    }

    @Override
    public List<Blob> listBlobs() {
        List<String> uuids = m_blobStoreProvider.listBlobUUIDs();
        List<Blob> blobs = new ArrayList<Blob>();
        for (String uuid : uuids) {
            try {
                // assume existence and do not eagerly init headers
                blobs.add(internalGetBlob(uuid, false));
            } catch (BlobStoreException e) {
                throw new IllegalStateException("Exception not expected with initHeaders false...", e);
            }
        }
        return blobs;
    }

    @Override
    public List<Blob> findBlobs(String filter) throws BlobStoreException {

        Filter filt = null;
        try {
            filt = FrameworkUtil.createFilter(filter);
        } catch (InvalidSyntaxException e) {
            throw new InvalidFilterException(e.getMessage(), e);
        }

        List<Blob> blobs = listBlobs();
        List<Blob> result = new ArrayList<Blob>();
        for (Blob blob : blobs) {
            try {
                if (filt.matches(blob.getHeaders())) {
                    result.add(blob);
                }
            } catch (NoSuchBlobException e) {
                m_logService.log(LogService.LOG_DEBUG,
                        "MoSuchBlobException while retrieving headers. Probably deleted in other thread...");
            }
        }
        return result;
    }

    @Override
    public Blob addBlob(InputStream dataInput, String contentType) throws BlobStoreException {

        // TODO real UUID
        String blobUUID = UUID.randomUUID().toString();
        String timeStamp = String.valueOf(System.currentTimeMillis());

        Blob blob = null;
        CountingInputStream cis = null;
        DigestInputStream dis = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            dis = new DigestInputStream(dataInput, md);
            cis = new CountingInputStream(dis);

            Map<String, String> headers = m_blobStoreProvider.addBlob(blobUUID, cis);
            headers.put(BlobHeader.UUID.headerKey(), blobUUID);
            headers.put(BlobHeader.CONTENTTYPE.headerKey(), contentType);
            headers.put(BlobHeader.LASTACCESSED.headerKey(), timeStamp);
            headers.put(BlobHeader.CREATED.headerKey(), timeStamp);
            headers.put(BlobHeader.LASTMODIFIED.headerKey(), timeStamp);
            headers.put(BlobHeader.CONTENTMD5.headerKey(),
                    new BigInteger(1, dis.getMessageDigest().digest()).toString(16));
            headers.put(BlobHeader.CONTENTSIZE.headerKey(), String.valueOf(cis.getCount()));

            headers = m_blobStoreProvider.setBlobHeaders(blobUUID, headers);
            blob = internalAddBlob(blobUUID, headers);

        } catch (NoSuchAlgorithmException e) {
            m_logService.log(LogService.LOG_ERROR, "Failed to create MD5 MessageDigest");
            throw new BlobStoreException("Failed to create MD5 MessageDigest", e);
        } catch (NoSuchBlobException e) {
            m_logService.log(LogService.LOG_DEBUG, "NoSuchBlobException while setting headers. Probably deleted...");
            throw new BlobStoreException("Failed to create blob", e);
        } finally {
            try {
                if (cis != null)
                    cis.close();
                if (dis != null)
                    dis.close();
            } catch (IOException e) {
                m_logService.log(LogService.LOG_WARNING, "Failed to close stream", e);
            }
        }
        return blob;
    }

    @Override
    public Blob getBlob(String blobUUID) throws BlobStoreException {
        // assure existence by eager init headers
        BlobImpl blob = internalGetBlob(blobUUID, true);
        return blob;
    }

    /**
     * Retrieves the headers for a blob calling the provider.
     * 
     * @param blobUUID
     *            The blob uuid
     * @return The headers
     * @throws NoSuchBlobException
     *             If the providers throws an exception
     */
    Map<String, String> internalGetBlobHeaders(String blobUUID) throws BlobStoreException {
        return m_blobStoreProvider.getBlobHeaders(blobUUID);
    }

    /**
     * Updates the headers for a blob by calling the provider. This method
     * guards and updates the system headers.
     * 
     * @param blob
     *            The blob
     * @param update
     *            The update headers
     * @param retainExisting
     *            Flag indicating whether to retain existing headers
     * @return The new headers
     * @throws NoSuchBlobException
     */
    /**
     * @param blob
     * @param update
     * @param retainExisting
     * @throws BlobStoreException
     */
    void internalUpdateBlobHeaders(BlobImpl blob, Map<String, String> update, boolean retainExisting)
            throws BlobStoreException {

        Map<String, String> currentHeaders = blob.internalGetHeaders();
        Map<String, String> updatedHeaders = new HashMap<String, String>();
        String timeStamp = String.valueOf(System.currentTimeMillis());

        if (retainExisting) {
            updatedHeaders.putAll(currentHeaders);
        }
        updatedHeaders.putAll(update);

        for (BlobHeader header : BlobHeader.values()) {
            if (header == BlobHeader.LASTACCESSED || header == BlobHeader.LASTMODIFIED) {
                updatedHeaders.put(header.headerKey(), timeStamp);
            } else {
                updatedHeaders.put(header.headerKey(), currentHeaders.get(header.headerKey()));
            }
        }

        updatedHeaders = m_blobStoreProvider.setBlobHeaders(blob.getUUID(), updatedHeaders);
        blob.internalSetHeaders(updatedHeaders);

        for (BlobStoreListener listener : m_listeners) {
            try {
                listener.blobUpdated(blob);
            } catch (Exception e) {
                m_logService.log(LogService.LOG_WARNING, "BlobStoreListener threw exception", e);
            }
        }
    }

    /**
     * Return the input for a blob by calling the provider.
     * 
     * @param blobUUID
     *            The blob uuid
     * @return The input stream
     * @throws NoSuchBlobException
     */
    InputStream internalGetBlobInput(String blobUUID) throws BlobStoreException {
        return m_blobStoreProvider.getBlobInput(blobUUID);
    }

    /**
     * Removed a blob by calling the provider and clears it from the cache.
     * 
     * @param blob
     *            The blob
     * @throws NoSuchBlobException
     *             If the provider throws the exception.
     */
    void internalRemoveBlob(BlobImpl blob) throws BlobStoreException {

        m_blobCache.remove(blob.internalGetUUID());
        m_blobStoreProvider.remove(blob.internalGetUUID());

        for (BlobStoreListener listener : m_listeners) {
            try {
                listener.blobRemoved(blob);
            } catch (Exception e) {
                m_logService.log(LogService.LOG_WARNING, "BlobStoreListener threw exception", e);
            }
        }
    }

    /**
     * Creates a blob with the specified headers.
     * 
     * @param blobUUID
     *            The blob uuid
     * @param headers
     *            The headers, or <code>null</code>
     * @return The blob
     */
    BlobImpl internalAddBlob(String blobUUID, Map<String, String> headers) {
        BlobImpl blob = new BlobImpl(blobUUID, this, headers);
        blob = m_blobCache.putIfAbsent(blobUUID, blob);

        for (BlobStoreListener listener : m_listeners) {
            try {
                listener.blobAdded(blob);
            } catch (Exception e) {
                m_logService.log(LogService.LOG_WARNING, "BlobStoreListener threw exception", e);
            }
        }
        return blob;
    }

    /**
     * Retrieves or constructs a blob. Header initializing is optional for
     * performance reasons in list/find scenarios.
     * 
     * @param blobUUID
     *            The blob uuid
     * @param initHeaders
     *            Indicates whether to init headers
     * @return The blob
     * @throws NoSuchBlobException
     *             If thrown by header initialization
     */
    BlobImpl internalGetBlob(String blobUUID, boolean initHeaders) throws BlobStoreException {
        BlobImpl blob = (BlobImpl) m_blobCache.get(blobUUID);
        if (blob == null) {
            Map<String, String> headers = null;
            if (initHeaders) {
                headers = internalGetBlobHeaders(blobUUID);
            }
            blob = new BlobImpl(blobUUID, this, headers);
            blob = m_blobCache.putIfAbsent(blobUUID, blob);
        }
        return blob;
    }
}
