/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.amdatu.blobstore.BlobStore;
import org.amdatu.blobstore.core.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

/**
 * BlobStores registry delegate for {@link BlobStoresResource}
 * 
 */
class BlobStoresRegistry {

    // TODO robustness service changed?

    private final Map<String, BlobStoreRegistration> m_blobStores = new HashMap<String, BlobStoreRegistration>();
    private volatile LogService m_logService;

    BlobStoresRegistry() {
    }

    void addBlobStore(ServiceReference<BlobStore> reference, BlobStore blobStore) {
        String storeID = (String) reference.getProperty(Constants.BLOBSTORE_STOREID);
        String restPath = (String) reference.getProperty(Constants.BLOBSTORE_RESTPATH);
        if (restPath == null || restPath.equals("")) {
            m_logService.log(LogService.LOG_INFO, "Ingoring BlobStore " + storeID + " without restPath property");
            return;
        }

        synchronized (m_blobStores) {
            if (m_blobStores.containsKey(restPath)) {
                m_logService.log(LogService.LOG_WARNING, "Ingoring BlobStore  " + storeID + " with duplicate restPath "
                        + restPath);
                return;
            }
            m_blobStores.put(restPath, new BlobStoreRegistration(blobStore, restPath));
        }
        m_logService.log(LogService.LOG_INFO, "Added BlobStore  " + storeID + " at path " + restPath);
    }

    void removeBlobProvider(ServiceReference<BlobStore> reference, BlobStore blobStore) {
        String storeID = (String) reference.getProperty(Constants.BLOBSTORE_STOREID);
        String restPath = (String) reference.getProperty(Constants.BLOBSTORE_RESTPATH);
        synchronized (m_blobStores) {
            m_blobStores.remove(restPath);
        }
        m_logService.log(LogService.LOG_INFO, "Removed BlobStore  " + storeID + " at path " + restPath);
    }

    List<BlobStoreRegistration> listStores() {
        synchronized (m_blobStores) {
            List<BlobStoreRegistration> stores = new ArrayList<BlobStoreRegistration>(m_blobStores.values());
            return stores;
        }
    }

    BlobStoreRegistration getStore(String storePath) {
        synchronized (m_blobStores) {
            BlobStoreRegistration store = m_blobStores.get(storePath);
            return store;
        }
    }
}
