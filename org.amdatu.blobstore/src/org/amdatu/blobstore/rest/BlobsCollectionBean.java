/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore.rest;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.amdatu.blobstore.Blob;

/**
 * JAXB bean for a Blob entity.
 * 
 */
@XmlRootElement(name = "blobs")
@XmlAccessorType(XmlAccessType.FIELD)
public class BlobsCollectionBean {

    public List<BlobsEntryBean> blobs;

    public static class BlobsEntryBean {
        public String blobUUID;
        public URI location;
    }

    public static BlobsCollectionBean from(List<Blob> blobs, UriBuilder uriBuilder) {
        BlobsCollectionBean bean = new BlobsCollectionBean();
        bean.blobs = new ArrayList<BlobsEntryBean>();
        for (Blob blob : blobs) {
            BlobsEntryBean entry = new BlobsEntryBean();
            entry.blobUUID = blob.getUUID();
            entry.location = uriBuilder.build(blob.getUUID());
            bean.blobs.add(entry);
        }
        return bean;
    }
}
