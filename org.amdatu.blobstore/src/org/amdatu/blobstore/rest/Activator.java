/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore.rest;

import java.util.Properties;

import org.amdatu.blobstore.BlobStore;
import org.amdatu.blobstore.core.Constants;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

/**
 * The OSGi bundle activator for Amdatu BlobStore REST support. This activator
 * registers a JAX-RS root resource at {@link BlobStoresResource#BASE_PATH}. The
 * root resource provides a sub resource for every {@link BlobStore} service
 * registration with a valid {@link Constants#BLOBSTORE_RESTPATH} property.
 * 
 */
public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {

        final BlobStoresRegistry registry = new BlobStoresRegistry();
        final BlobStoresResource resource = new BlobStoresResource(registry);

        final Object composition = new Object() {

            final Object[] m_objects = new Object[] { registry, resource };

            @SuppressWarnings("unused")
            public Object[] getComposition() {
                return m_objects;
            }
        };

        Properties properties = new Properties();
        manager.add(createComponent()
                .setInterface(Object.class.getName(), properties)
                .setImplementation(resource)
                .setComposition(composition, "getComposition")
                .add(createServiceDependency().setService(BlobStore.class, "(" + Constants.BLOBSTORE_RESTPATH + "=*)")
                        .setRequired(false).setCallbacks("addBlobStore", "removeBlobStore"))
                .add(createServiceDependency().setService(LogService.class).setRequired(false)));

    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
    }
}
