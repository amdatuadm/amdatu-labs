/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore.rest;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * JAXB bean for a BlobStores collection entity.
 * 
 */
@XmlRootElement(name = "blobstores")
@XmlAccessorType(XmlAccessType.FIELD)
public class BlobStoresCollectionBean {

    public List<BlobStoresEntryBean> stores;

    public static class BlobStoresEntryBean {
        public String storeID;
        public String providerType;
        public String description;
        public URI location;
    }

    public static BlobStoresCollectionBean from(List<BlobStoreRegistration> registrations, UriBuilder uriBuilder) {

        BlobStoresCollectionBean bean = new BlobStoresCollectionBean();
        bean.stores = new ArrayList<BlobStoresEntryBean>();

        for (BlobStoreRegistration registration : registrations) {
            BlobStoresEntryBean entry = new BlobStoresEntryBean();
            entry.storeID = registration.getStore().getID();
            entry.providerType = registration.getStore().getProviderType();
            entry.description = registration.getStore().getDescription();
            entry.location = uriBuilder.build(registration.getPath());
            bean.stores.add(entry);
        }
        return bean;
    }
}
