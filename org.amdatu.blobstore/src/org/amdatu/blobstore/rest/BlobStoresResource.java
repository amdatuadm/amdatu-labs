/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore.rest;

import java.io.File;
import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.amdatu.blobstore.Blob;
import org.amdatu.blobstore.BlobStoreException;
import org.amdatu.blobstore.InvalidFilterException;
import org.amdatu.blobstore.NoSuchBlobException;
import org.amdatu.storage.blobstore.BlobStoreService;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.osgi.service.log.LogService;

/**
 * JAX-RS root-resource tracking {@link BlobStoreService} registrations.
 * 
 */
@Path(BlobStoresResource.BASE_PATH)
public class BlobStoresResource {

    // TODO serve system headers through HTTP on GET/HEAD
    // TODO properly manage etag & lastmodified
    // TODO support If-Modified-Since

    public static final String BASE_PATH = "blobstore";
    private volatile LogService m_logService;

    private final BlobStoresRegistry m_registry;

    public BlobStoresResource(BlobStoresRegistry registry) {
        m_registry = registry;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBlobStores(@Context UriInfo urifInfo) {

        UriBuilder uriBuilder = urifInfo.getAbsolutePathBuilder().path("{store}");
        try {

            List<BlobStoreRegistration> stores = listStores();
            BlobStoresCollectionBean bean = BlobStoresCollectionBean.from(stores, uriBuilder);

            return Response.ok(bean).header(HttpHeaders.ETAG, "etag" + System.currentTimeMillis())
                    .lastModified(new Date()) //
                    .expires(new Date()) //
                    .build();

        } catch (Exception e) {
            return handleException(e);
        }
    }

    @HEAD
    @Produces(MediaType.APPLICATION_JSON)
    public Response headBlobStores() {

        return Response.ok().header(HttpHeaders.ETAG, "etag" + System.currentTimeMillis()).lastModified(new Date()) //
                .expires(new Date()) //
                .build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{store}")
    public Response getBlobStore(@Context UriInfo uriInfo, @PathParam("store") String storePath,
            @QueryParam("filter") @DefaultValue("") String filterParam) {

        BlobStoreRegistration holder = getStore(storePath);
        if (holder == null)
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).build();

        try {
            List<Blob> blobs = null;
            if (!filterParam.equals("")) {
                blobs = holder.getStore().findBlobs(filterParam);
            } else {
                blobs = holder.getStore().listBlobs();
            }

            UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder().path("{blob}");
            BlobsCollectionBean bean = BlobsCollectionBean.from(blobs, uriBuilder);

            return Response.ok(bean) //
                    .header(HttpHeaders.ETAG, "etag" + System.currentTimeMillis()) //
                    .lastModified(new Date()) //
                    .expires(new Date()) //
                    .build();

        } catch (Exception e) {
            return handleException(e);
        }
    }

    @HEAD
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{store}")
    public Response headBlobStore(@PathParam("store") String storePath) {

        BlobStoreRegistration holder = getStore(storePath);
        if (holder == null)
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).build();

        return Response.ok() //
                .header(HttpHeaders.ETAG, "etag" + System.currentTimeMillis()).lastModified(new Date()) //
                .expires(new Date()) //
                .build();
    }

    @SuppressWarnings("unchecked")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("{store}")
    public Response createBlob(@PathParam("store") String storePath, @Context HttpServletRequest request) {

        BlobStoreRegistration holder = getStore(storePath);
        if (holder == null)
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).build();

        List<FileItem> items = null;
        try {

            FileItemFactory factory = new DiskFileItemFactory(100000, new File(System.getProperty("java.io.tmpdir")));
            ServletFileUpload upload = new ServletFileUpload(factory);
            items = upload.parseRequest(request);

            if (items.size() == 0) {
                return Response.status(Response.Status.BAD_REQUEST).entity("No multipart part found: " + storePath)
                        .build();
            }
            if (items.size() > 1) {
                return Response.status(Response.Status.BAD_REQUEST)
                        .entity("Multiple multipart parts found: " + storePath).build();
            }
            FileItem item = items.get(0);

            Blob blob = holder.getStore().addBlob(item.getInputStream(), item.getContentType());

            URI location = UriBuilder.fromResource(BlobStoresResource.class).path("{blobstoreID}/{blobID}")
                    .build(storePath, blob.getUUID());

            return Response.created(location) //
                    .build();

        } catch (Exception e) {
            return handleException(e);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{store}/{blobUUID}")
    public Response getBlobMetaData(@PathParam("store") String storePath, @PathParam("blobUUID") String blobUUID) {

        BlobStoreRegistration holder = getStore(storePath);
        if (holder == null)
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).build();

        try {
            Blob blob = holder.getStore().getBlob(blobUUID);
            if (blob == null)
                return Response.status(Response.Status.NOT_FOUND).entity("No such blob: " + blobUUID).build();

            Map<String, String> headers = blob.getHeaders();
            BlobHeadersBean bean = BlobHeadersBean.from(headers);

            return Response.ok(bean).header(HttpHeaders.ETAG, blob.getContentMD5())
                    .lastModified(blob.getLastModified()).expires(new Date(System.currentTimeMillis() + 60000)).build();

        } catch (Exception e) {
            return handleException(e);
        }
    }

    @GET
    @Produces(MediaType.MEDIA_TYPE_WILDCARD)
    @Path("{store}/{blobUUID}")
    public Response getBlobPayload(@PathParam("store") String storePath, @PathParam("blobUUID") String blobUUID) {

        BlobStoreRegistration registration = getStore(storePath);
        if (registration == null)
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).build();

        try {
            Blob blob = registration.getStore().getBlob(blobUUID);
            if (blob == null)
                return Response.status(Response.Status.NOT_FOUND).entity("No such blob: " + blobUUID).build();

            return Response.ok(blob.getContent()).type(blob.getContentType())
                    .header(HttpHeaders.ETAG, blob.getContentMD5()).lastModified(blob.getLastModified())
                    .expires(new Date(System.currentTimeMillis() + 60000)).build();

        } catch (Exception e) {
            return handleException(e);
        }
    }

    @HEAD
    @Path("{store}/{blobUUID}")
    public Response headBlob(@PathParam("store") String storePath, @PathParam("blobUUID") String blobUUID) {

        BlobStoreRegistration registration = getStore(storePath);
        if (registration == null)
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).build();

        try {
            Blob blob = registration.getStore().getBlob(blobUUID);
            if (blob == null)
                return Response.status(Response.Status.NOT_FOUND).entity("No such blob: " + blobUUID).build();

            return Response.ok().type(blob.getContentType()).header(HttpHeaders.ETAG, blob.getContentMD5())
                    .lastModified(blob.getLastModified()).expires(new Date(System.currentTimeMillis() + 60000)).build();

        } catch (Exception e) {
            return handleException(e);
        }
    }

    @PUT
    @Path("{store}/{blobUUID}")
    public Response updateBlob(@PathParam("store") String storePath, @PathParam("blobUUID") String blobUUID,
            BlobHeadersBean headers) {

        BlobStoreRegistration registration = getStore(storePath);
        if (registration == null)
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).build();

        try {

            Blob blob = registration.getStore().getBlob(blobUUID);
            if (blob == null)
                return Response.status(Response.Status.NOT_FOUND).entity("No such blob: " + blobUUID).build();

            blob.updateHeaders(headers.headers, false);

            return Response.noContent().type(blob.getContentType()).header(HttpHeaders.ETAG, blob.getContentMD5())
                    .lastModified(blob.getLastModified()).expires(new Date(System.currentTimeMillis() + 60000)).build();

        } catch (Exception e) {
            return handleException(e);
        }
    }

    @DELETE
    @Path("{store}/{blobUUID}")
    public Response deleteBlob(@PathParam("store") String storePath, @PathParam("blobUUID") String blobUUID) {

        BlobStoreRegistration registration = getStore(storePath);
        if (registration == null)
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).build();

        try {
            Blob blob = registration.getStore().getBlob(blobUUID);
            if (blob == null)
                return Response.status(Response.Status.NOT_FOUND).entity("No such blob: " + blobUUID).build();

            blob.remove();

            return Response.noContent() //
                    .build();

        } catch (Exception e) {
            return handleException(e);
        }
    }

    private List<BlobStoreRegistration> listStores() {
        return m_registry.listStores();
    }

    private BlobStoreRegistration getStore(String storePath) {
        return m_registry.getStore(storePath);
    }

    private Response handleException(Exception exception) {
        if (exception instanceof NoSuchBlobException) {
            return Response.status(Response.Status.NOT_FOUND).entity(exception.getMessage()).build();
        }
        if (exception instanceof InvalidFilterException) {
            return Response.status(Response.Status.BAD_REQUEST).entity(exception.getMessage()).build();
        }
        if (exception instanceof BlobStoreException) {
            m_logService.log(LogService.LOG_WARNING, "BlobStore threw exception", exception);
            return Response.serverError().entity(exception.getMessage()).build();
        }
        m_logService.log(LogService.LOG_WARNING, "Uncaught exception", exception);
        return Response.serverError().build();
    }

}
