/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore.spi;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.amdatu.blobstore.core.Constants;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;

/**
 * Convenience base class for provider specific service factories. This base
 * ensures that the required configuration properties are specified manages
 * component life-cycle.
 * 
 */
public abstract class BlobStoreProviderFactoryBase implements ManagedServiceFactory {

    public static final String KEY_IDENTIFIER = "identifier";
    public static final String KEY_CONTAINER = "container";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_RESTPATH = "restpath";

    private final Object COMPONENTS_LOCK = new Object();
    private final Map<String, Component> m_components = new HashMap<String, Component>();

    private volatile DependencyManager m_dependencyManager;
    private volatile LogService m_logService;

    @Override
    public final void updated(String pid, Dictionary<String, ?> configuration) throws ConfigurationException {

        if (pid == null || pid.equals("")) {
            m_logService.log(LogService.LOG_ERROR, "Ignoring configuration update called without PID");
            return;
        }

        if (configuration == null) {
            m_logService.log(LogService.LOG_ERROR, "Ignoring configuration update called without properties");
            return;
        }

        String identifier = (String) configuration.get(Constants.BLOBSTORE_STOREID);
        if (identifier == null || "".equals(identifier)) {
            m_logService.log(LogService.LOG_ERROR, "Ignoring configration update for PID '" + pid
                    + "'. Missing required property '" + KEY_IDENTIFIER + "'");
            return;
        }

        String description = (String) configuration.get(Constants.BLOBSTORE_DESCRIPTION);
        if (description == null)
            description = "No description configured";

        String restpath = (String) configuration.get(Constants.BLOBSTORE_RESTPATH);
        if (restpath == null)
            restpath = "";

        Dictionary<String, Object> properties = new Hashtable<String, Object>();
        properties.put(Constants.BLOBSTORE_STOREID, identifier);
        properties.put(Constants.BLOBSTORE_DESCRIPTION, description);
        properties.put(Constants.BLOBSTORE_RESTPATH, restpath);
        properties.put(Constants.BLOBSTORE_PROVIDERTYPE, getProviderType());

        Component component = createComponent(m_dependencyManager, configuration, properties);
        if (component == null) {
            return;
        }

        Component oldComponent = null;
        synchronized (COMPONENTS_LOCK) {
            oldComponent = m_components.put(pid, component);
        }
        if (oldComponent != null) {
            m_dependencyManager.remove(oldComponent);
        }
        m_dependencyManager.add(component);
        m_logService.log(LogService.LOG_INFO, "Updated " + getName() + " BlobStoreProvider for PID '" + pid + "'");
    }

    @Override
    public final void deleted(String pid) {

        if (pid == null || pid.equals("")) {
            m_logService.log(LogService.LOG_ERROR, "Ignoring configuration update called without PID");
            return;
        }

        Component oldComponent = null;
        synchronized (COMPONENTS_LOCK) {
            oldComponent = m_components.remove(pid);
        }

        if (oldComponent != null) {
            m_logService.log(LogService.LOG_INFO, "Removed InMem BlobStoreProvider for PID '" + pid + "'");
            m_dependencyManager.remove(oldComponent);
        }
    }

    /**
     * Returns the dependencyManager.
     * 
     * @return the dependencyManager
     */
    protected DependencyManager getDependencyManager() {
        return m_dependencyManager;
    }

    /**
     * Returns the logService.
     * 
     * @return the logService
     */
    protected LogService getLogService() {
        return m_logService;
    }

    /**
     * Returns the unique PID for this factory.
     * 
     * @return The PID
     */
    protected abstract String getFactoryPID();

    /**
     * Returns the unique provider type for this factory.
     * 
     * @return The type
     */
    protected abstract String getProviderType();

    /**
     * Creates a BlobStore Component for the specified configuration. The
     * provided service properties contain the required base properties, but an
     * implementation may augment them.
     * 
     * @param manager
     *            The manager
     * @param properties
     *            The configuration properties.
     * @param properties
     *            The service properties.
     * 
     * @return The component, or <code>null</code>
     */
    protected abstract Component createComponent(DependencyManager manager, Dictionary<String, ?> configuration,
            Dictionary<String, Object> properties);
}
