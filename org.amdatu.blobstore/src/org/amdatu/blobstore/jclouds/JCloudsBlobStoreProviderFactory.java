/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore.jclouds;

import static org.amdatu.blobstore.jclouds.Constants.JCLOUDS_BLOBSTORE_CONTAINER;
import static org.amdatu.blobstore.jclouds.Constants.JCLOUDS_FACTORY_PID;
import static org.amdatu.blobstore.jclouds.Constants.JCLOUDS_PROVIDER_TYPE;
import static org.amdatu.blobstore.jclouds.Constants.JCLOUDS_SERVICE_IDENTIFIER;

import java.util.Dictionary;

import org.amdatu.blobstore.core.Constants;
import org.amdatu.blobstore.spi.BlobStoreProvider;
import org.amdatu.blobstore.spi.BlobStoreProviderFactoryBase;
import org.amdatu.storage.blobstore.BlobStoreService;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.service.log.LogService;

/**
 * Managed service factory for {@link JCloudsBlobStoreProvider} using PID
 * {@link JCloudsBlobStoreProviderFactory#JCLOUDS_FACTORY_PID}.
 */
public class JCloudsBlobStoreProviderFactory extends BlobStoreProviderFactoryBase {

    @Override
    public String getName() {
        return JCLOUDS_PROVIDER_TYPE;
    }

    @Override
    protected String getFactoryPID() {
        return JCLOUDS_FACTORY_PID;
    }

    @Override
    protected String getProviderType() {
        return JCLOUDS_PROVIDER_TYPE;
    }

    @Override
    protected Component createComponent(DependencyManager manager, Dictionary<String, ?> configuration,
            Dictionary<String, Object> properties) {

        String identifier = (String) configuration.get(Constants.BLOBSTORE_STOREID);
        String description = (String) configuration.get(Constants.BLOBSTORE_DESCRIPTION);

        identifier += "";
        String container = (String) configuration.get(JCLOUDS_BLOBSTORE_CONTAINER);
        if (container == null) {
            getLogService().log(LogService.LOG_WARNING, "Missing required configuraion parameter: container");
            return null;
        }

        properties.put(JCLOUDS_BLOBSTORE_CONTAINER, container);

        Component component = manager
                .createComponent()
                .setInterface(BlobStoreProvider.class.getName(), properties)
                .setImplementation(
                        new JCloudsBlobStoreProvider(identifier, JCLOUDS_PROVIDER_TYPE, description, container))
                .add(manager.createServiceDependency()
                        .setService(BlobStoreService.class, "(" + JCLOUDS_SERVICE_IDENTIFIER + "=" + identifier + ")")
                        .setRequired(true))
                .add(manager.createServiceDependency().setService(LogService.class).setRequired(false));

        return component;
    }
}
