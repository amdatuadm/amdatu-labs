package org.amdatu.blobstore.jclouds;

import org.amdatu.storage.blobstore.BlobStoreService;

public interface Constants {

    /**
     * Factory PID for the JClouds blobStore provider factory.
     */
    String JCLOUDS_FACTORY_PID = "org.amdatu.blobstore.jclouds.factory";

    /**
     * Provider type for the JClouds blobStore provider.
     */
    String JCLOUDS_PROVIDER_TYPE = "JClouds";

    /**
     * Configuration and ServiceRegistration property for the JClouds container.
     */
    String JCLOUDS_BLOBSTORE_CONTAINER = "org.amdatu.blobstore.jclouds.container";

    /**
     * JClouds {@link BlobStoreService} identifier property
     */
    String JCLOUDS_SERVICE_IDENTIFIER = "id";
}
