/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore.jclouds;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.amdatu.blobstore.NoSuchBlobException;
import org.amdatu.blobstore.spi.BlobStoreProvider;
import org.amdatu.storage.blobstore.BlobStoreService;
import org.jclouds.blobstore.BlobStoreContext;
import org.jclouds.blobstore.domain.Blob;
import org.jclouds.blobstore.domain.PageSet;
import org.jclouds.blobstore.domain.StorageMetadata;
import org.osgi.service.log.LogService;

/**
 * Amdatu BlobStore provider implementation for a Amdatu JClouds BlobStore
 * storage service.
 * 
 */
class JCloudsBlobStoreProvider implements BlobStoreProvider {

    private final String m_storeID;
    private final String m_type;
    private final String m_description;
    private final String m_containerID;

    private volatile BlobStoreService m_blobStoreService;
    private volatile LogService m_logService;

    private volatile BlobStoreContext m_blobStoreContext;

    JCloudsBlobStoreProvider(String storeID, String type, String description, String containerID) {
        m_storeID = storeID;
        m_type = type;
        m_description = description;
        m_containerID = containerID;
    }

    void start() {
        m_blobStoreContext = m_blobStoreService.getContext();
        if (!m_blobStoreContext.getBlobStore().containerExists(m_containerID)) {
            m_blobStoreContext.getBlobStore().createContainerInLocation(null, m_containerID);
        }
    }

    void stop() {
        m_blobStoreContext.close();
        m_blobStoreContext = null;
    }

    @Override
    public String getStoreID() {
        return m_storeID;
    }

    @Override
    public String getProviderType() {
        return m_type;
    }

    @Override
    public String getDescription() {
        return m_description;
    }

    @Override
    public List<String> listBlobUUIDs() {
        List<String> result = new ArrayList<String>();
        PageSet<? extends StorageMetadata> blobs = m_blobStoreContext.getBlobStore().list(m_containerID);
        Iterator<? extends StorageMetadata> iterator = blobs.iterator();
        while (iterator.hasNext()) {
            StorageMetadata metadata = iterator.next();
            result.add(metadata.getName());
        }
        return result;
    }

    @Override
    public Map<String, String> addBlob(String blobUUID, InputStream dataInput) {

        Blob blob = m_blobStoreContext.getBlobStore().blobBuilder(blobUUID).payload(dataInput).build();
        m_blobStoreContext.getBlobStore().putBlob(m_containerID, blob);

        Map<String, String> headers = new HashMap<String, String>();
        return headers;
    }

    @Override
    public InputStream getBlobInput(String blobUUID) throws NoSuchBlobException {
        Blob blob = blob(blobUUID);
        return blob.getPayload().getInput();
    }

    @Override
    public Map<String, String> getBlobHeaders(String blobUUID) throws NoSuchBlobException {
        Blob blob = blob(blobUUID);
        Map<String, String> headers = new HashMap<String, String>();
        headers.putAll(blob.getMetadata().getUserMetadata());
        return headers;
    }

    @Override
    public Map<String, String> setBlobHeaders(String blobUUID, Map<String, String> headers) throws NoSuchBlobException {
        Blob blob = blob(blobUUID);
        blob.getMetadata().getUserMetadata().putAll(headers);
        return headers;
    }

    @Override
    public void remove(String blobUUID) throws NoSuchBlobException {
        m_blobStoreContext.getBlobStore().removeBlob(m_containerID, blobUUID);
    }

    private Blob blob(String blobUUID) throws NoSuchBlobException {
        Blob blob = m_blobStoreContext.getBlobStore().getBlob(m_containerID, blobUUID);
        if (blob == null) {
            throw new NoSuchBlobException(blobUUID);
        }
        return blob;
    }
}
