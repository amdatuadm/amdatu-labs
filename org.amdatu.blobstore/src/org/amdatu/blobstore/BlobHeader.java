/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore;

/**
 * System headers for blobs. These headers are managed and can not be set
 * through {@link Blob#updateHeaders(java.util.Map, boolean).
 */
public enum BlobHeader {

    /**
     * UUID identifier (immutable).
     */
    UUID,

    /**
     * Timestamp that indicates the creation date (immutable).
     */
    CREATED,

    /**
     * Timestamp that indicates the last modification date.
     */
    LASTMODIFIED,

    /**
     * Timestamp that indicates the last access date.
     */
    LASTACCESSED,

    /**
     * Mimetype that indicates the type of binary content.
     */
    CONTENTTYPE,

    /**
     * MD5 hash of the binary content.
     */
    CONTENTMD5,

    /**
     * Size in number of bytes of the binary content.
     */
    CONTENTSIZE;

    private final String m_headerKey;

    private BlobHeader() {
        m_headerKey = "X-Amdatu-BlobStore-" + name().toLowerCase();
    }

    public String headerKey() {
        return m_headerKey;
    }
}
