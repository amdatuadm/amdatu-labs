/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore.events;

import java.util.Dictionary;
import java.util.Hashtable;

import org.amdatu.blobstore.Blob;
import org.amdatu.blobstore.BlobStore;
import org.amdatu.blobstore.BlobStoreListener;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

/**
 * Implementation of {@link BlobStoreListener} that posts events to
 * {@link EventAmdin} for all callbacks received.
 * 
 */
public class EventsEmitter implements BlobStoreListener {

    public static final String TOPIC_BASE = "org/amdatu/blobstore/";
    public static final String TOPIC_STORE = TOPIC_BASE + "store/";
    public static final String TOPIC_BLOB = TOPIC_BASE + "blob/";
    public static final String EVENT_STORE_REGISTERED = TOPIC_STORE + "ADDED";
    public static final String EVENT_STORE_UNREGISTERED = TOPIC_STORE + "REMOVED";
    public static final String EVENT_BLOB_ADDED = TOPIC_BLOB + "ADDED";
    public static final String EVENT_BLOB_UPDATED = TOPIC_BLOB + "UPDATED";
    public static final String EVENT_BLOB_REMOVED = TOPIC_BLOB + "REMOVED";

    public static final String EVENT_KEY = "event";
    public static final String STORE_KEY = "store";
    public static final String BLOB_KEY = "store";

    private volatile EventAdmin m_eventAdmin;

    @Override
    public void registered(BlobStore store) {
        postEvent(EVENT_STORE_REGISTERED, store, null);
    }

    @Override
    public void unregistered(BlobStore store) {
        postEvent(EVENT_STORE_UNREGISTERED, store, null);
    }

    @Override
    public void blobAdded(Blob blob) {
        postEvent(EVENT_BLOB_ADDED, null, blob);
    }

    @Override
    public void blobRemoved(Blob blob) {
        postEvent(EVENT_BLOB_REMOVED, null, blob);
    }

    @Override
    public void blobUpdated(Blob blob) {
        postEvent(EVENT_BLOB_UPDATED, null, blob);
    }

    private void postEvent(String topic, BlobStore store, Blob blob) {
        Dictionary<String, Object> properties = new Hashtable<String, Object>();
        if (store != null)
            properties.put(STORE_KEY, store);
        if (blob != null)
            properties.put(BLOB_KEY, blob);
        Event e = new Event(topic, properties);
        m_eventAdmin.postEvent(e);
    }
}
