/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore;

import java.io.InputStream;
import java.util.List;

/**
 * BlobStore interface.
 * 
 */
public interface BlobStore {

    /**
     * Returns the store identifier.
     * 
     * @return The identifier
     */
    String getID();

    /**
     * Returns the store provider type.
     * 
     * @return The provider type
     */
    String getProviderType();

    /**
     * Returns the store description.
     * 
     * @return The description
     */
    String getDescription();

    /**
     * Returns a list of all blobs.
     * 
     * @return A List of blobs
     */
    List<Blob> listBlobs() throws BlobStoreException;

    /**
     * Returns a list of all blobs that match the filter.
     * 
     * @param filter
     *            An RFC 1960-based Filter.
     * @return A list of blobs
     */
    List<Blob> findBlobs(String filter) throws BlobStoreException;

    /**
     * Creates a new blob.
     * 
     * @param dataInput
     *            An input stream
     * @param contentType
     *            A content type
     * @return The created blob
     */
    Blob addBlob(InputStream dataInput, String contentType) throws BlobStoreException;

    /**
     * Retrieves a blob.
     * 
     * @param blobUUID
     *            A blob uuid
     * @return An existing blob, or <code>null</code>
     */
    Blob getBlob(String blobUUID) throws BlobStoreException;
}
