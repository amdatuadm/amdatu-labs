/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore.util;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Internal cache using weak references. This provides retention of values as
 * long a they are referenced by someone. In some cases it may also improve
 * performance without the risk of keeping stale data around.
 */
public class ConcurrentWeakCache<KEY, VALUE> {

    private final ConcurrentHashMap<KEY, KeyReference<KEY, VALUE>> m_map = new ConcurrentHashMap<KEY, KeyReference<KEY, VALUE>>();
    private final KeyReferenceQueue<KEY, VALUE> m_referenceQueue = new KeyReferenceQueue<KEY, VALUE>();
    private final AtomicInteger m_modCount = new AtomicInteger(0);

    /**
     * Returns a value from the cache.
     * 
     * @param key
     *            The key
     * @return
     */
    public VALUE get(KEY key) {
        KeyReference<KEY, VALUE> ref = m_map.get(key);
        return ref == null ? null : ref.get();
    }

    /**
     * Puts a value in the cache if the key is not mapped.
     * 
     * @param key
     *            The key
     * @param value
     *            The value
     * @return The actual value in the cache
     */
    public VALUE putIfAbsent(KEY key, VALUE value) {
        cleanUp();
        // FIXME not 100% safe
        KeyReference<KEY, VALUE> ref = m_map.putIfAbsent(key,
                new KeyReference<KEY, VALUE>(key, value, m_referenceQueue));
        return ref == null ? value : ref.get();
    }

    /**
     * Removes a key from the cache.
     * 
     * @param key
     *            The key
     * @return The assigned value, or <code>null</code>
     */
    public VALUE remove(KEY key) {
        cleanUp();
        KeyReference<KEY, VALUE> ref = m_map.remove(key);
        if (ref != null) {
            return ref.get();
        }
        return null;
    }

    /**
     * Cleans the map by removing entries that have been collected.
     */
    private void cleanUp() {
        if (m_modCount.incrementAndGet() > 1000) {
            m_modCount.set(0);
            KeyReference<KEY, VALUE> ref;
            while ((ref = m_referenceQueue.poll()) != null) {
                m_map.remove(ref.getKEY());
            }
        }
    }

    /**
     * Generic weak reference that remembers the key.
     * 
     * @param <KEY>
     *            The key type
     * @param <VALUE>
     *            The value type
     */
    static class KeyReference<KEY, VALUE> extends WeakReference<VALUE> {

        private final KEY m_key;

        public KeyReference(KEY key, VALUE referent, ReferenceQueue<? super VALUE> queue) {
            super(referent, queue);
            m_key = key;
        }

        public KEY getKEY() {
            return m_key;
        }
    }

    /**
     * Generic reference queue that holds key references.
     * 
     * @param <KEY>
     *            The key type
     * @param <VALUE>
     *            The value type
     */
    static class KeyReferenceQueue<KEY, VALUE> extends ReferenceQueue<VALUE> {
        @Override
        @SuppressWarnings(value = "unchecked")
        public KeyReference<KEY, VALUE> poll() {
            return (KeyReference<KEY, VALUE>) super.poll();
        }
    }

}
