/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstore;

import java.io.InputStream;
import java.util.Date;
import java.util.Map;

/**
 * Blob interface for Amdatu BlobStore.
 * 
 */
public interface Blob {

    /**
     * Returns the store that manages this blob.
     * 
     * @return The store
     */
    BlobStore getStore();
    
    /**
     * Return the UUID.
     * 
     * @see BlobHeader#UUID
     * @return The uuid
     */
    String getUUID();

    /**
     * Returns the creation date.
     * 
     * @see BlobHeader#CREATED
     * @return The creation date
     * @throws NoSuchBlobException If the blob is no longer available
     */
    Date getCreated() throws BlobStoreException;

    /**
     * Returns the last accessed date.
     * 
     * @see BlobHeader#LASTACCESSED
     * @return The last accessed date
     * @throws NoSuchBlobException If the blob is no longer available
     */
    Date getLastAccessed() throws BlobStoreException;

    /**
     * Returns the last modified date.
     * 
     * @see BlobHeader#LASTMODIFIED
     * @return The last modified date
     * @throws NoSuchBlobException If the blob is no longer available
     */
    Date getLastModified() throws BlobStoreException;

    /**
     * Returns the binary size in number of bytes.
     * 
     * @see BlobHeader#CONTENTSIZE
     * @return The blob size
     * @throws NoSuchBlobException If the blob is no longer available
     */
    long getContentSize() throws BlobStoreException;

    /**
     * Returns the content type.
     * 
     * @see BlobHeader#CONTENTTYPE
     * @return type
     * @throws NoSuchBlobException If the blob is no longer available
     */
    String getContentType() throws BlobStoreException;

    /**
     * Returns the content MD5 hash.
     * 
     * @see BlobHeader#CONTENTMD5
     * @return The content md5
     * @throws NoSuchBlobException If the blob is no longer available
     */
    String getContentMD5() throws BlobStoreException;

    /**
     * Returns the content as a stream.
     * 
     * @return The content stream
     * @throws NoSuchBlobException If the blob is no longer available
     */
    InputStream getContent() throws BlobStoreException;

    /**
     * Returns a private copy of the current headers.
     * 
     * @return The headers
     * @throws NoSuchBlobException If the blob is no longer available
     */
    Map<String, String> getHeaders() throws BlobStoreException;

    /**
     * Update the headers.
     * 
     * @param headers The update headers
     * @param retainExisting indicates whether the retain existing headers.
     * @throws NoSuchBlobException If the blob is no longer available
     */
    void updateHeaders(Map<String, String> headers, boolean retainExisting) throws BlobStoreException;

    /**
     * Remove the blob.
     * 
     * @throws NoSuchBlobException If the blob is no longer available
     */
    void remove() throws BlobStoreException;
}
