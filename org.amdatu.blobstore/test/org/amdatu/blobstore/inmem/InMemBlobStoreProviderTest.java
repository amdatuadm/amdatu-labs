package org.amdatu.blobstore.inmem;

import org.amdatu.blobstore.core.AbstractBlobStoreTest;
import org.amdatu.blobstore.spi.BlobStoreProvider;

public class InMemBlobStoreProviderTest extends AbstractBlobStoreTest {

    @Override
    public BlobStoreProvider getBlobStoreProvider() {
        return new InMemBlobStoreProvider("store1", "inmem", "test store");
    }
}
