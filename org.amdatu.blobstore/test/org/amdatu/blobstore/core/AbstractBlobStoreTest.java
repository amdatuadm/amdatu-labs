package org.amdatu.blobstore.core;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import junit.framework.TestCase;

import org.amdatu.blobstore.Blob;
import org.amdatu.blobstore.BlobHeader;
import org.amdatu.blobstore.BlobStore;
import org.amdatu.blobstore.spi.BlobStoreProvider;

public abstract class AbstractBlobStoreTest extends TestCase {

    private BlobStore m_blobStore;

    public void setUp() throws Exception {
        BlobStoreProvider provider = getBlobStoreProvider();
        m_blobStore = new BlobStoreImpl(provider);
    }

    public abstract BlobStoreProvider getBlobStoreProvider();

    public BlobStore getBlobStore() {
        return m_blobStore;
    }

    public InputStream getRandomInputStream() {
        return new ByteArrayInputStream(UUID.randomUUID().toString().getBytes());
    }

    // tests

    public void testBlobCreateDelete() throws Exception {

        BlobStore store = getBlobStore();
        List<Blob> blobs = store.listBlobs();
        assertTrue("Store should be empty", blobs.size() == 0);

        Blob blob1 = store.addBlob(getRandomInputStream(), "application/vnd.amdatu.x");
        assertNotNull("Blob1 should not be null", blob1);

        blobs = store.listBlobs();
        assertTrue("Store should contain one blob", blobs.size() == 1);
        assertTrue("Store should contain blob1", blobs.contains(blob1));

        Blob blob2 = store.addBlob(getRandomInputStream(), "application/vnd.amdatu.x");
        assertNotNull("Blob2 should not be null", blob2);

        blobs = store.listBlobs();
        assertTrue("Store should contain two blobs", blobs.size() == 2);
        assertTrue("Store should contain blob1", blobs.contains(blob1));
        assertTrue("Store should contain blob2", blobs.contains(blob2));

        blob1.remove();

        blobs = store.listBlobs();
        assertTrue("Store should contain one blob", blobs.size() == 1);
        assertTrue("Store should contain blob2", blobs.contains(blob2));

        blob2.remove();

        blobs = store.listBlobs();
        assertTrue("Store should be empty", blobs.size() == 0);
    }

    public void testBlobHeaders() throws Exception {

        BlobStore store = getBlobStore();
        List<Blob> blobs = store.listBlobs();
        assertTrue("Store should be empty", blobs.size() == 0);

        Blob blob1 = store.addBlob(getRandomInputStream(), "application/vnd.amdatu.x");
        assertNotNull("Blob1 should not be null", blob1);

        // new blob must have all system headers
        assertNotNull("Headers should not be null", blob1.getHeaders());
        assertNotNull("UUID should not be null", blob1.getUUID());
        assertEquals("UUID should match header", blob1.getUUID(), blob1.getHeaders().get(BlobHeader.UUID.headerKey()));
        assertNotNull("Accessed should not be null", blob1.getLastAccessed());
        assertEquals("Accessed should match header", blob1.getLastAccessed(),
                new Date(Long.parseLong(blob1.getHeaders().get(BlobHeader.LASTACCESSED.headerKey()))));
        assertNotNull("Created should not be null", blob1.getCreated());
        assertEquals("Created should match header", blob1.getCreated(),
                new Date(Long.parseLong(blob1.getHeaders().get(BlobHeader.CREATED.headerKey()))));
        assertNotNull("Modified should not be null", blob1.getLastModified());
        assertEquals("Modified should match header", blob1.getLastModified(),
                new Date(Long.parseLong(blob1.getHeaders().get(BlobHeader.LASTMODIFIED.headerKey()))));
        assertNotNull("MD5 should not be null", blob1.getContentMD5());
        assertEquals("MD5 should match header", blob1.getContentMD5(),
                blob1.getHeaders().get(BlobHeader.CONTENTMD5.headerKey()));
        assertNotNull("ContentType should not be null", blob1.getContentType());
        assertEquals("ContentType should match header", blob1.getContentType(),
                blob1.getHeaders().get(BlobHeader.CONTENTTYPE.headerKey()));
        assertFalse("Size should not be -1", blob1.getContentSize() == -1);
        assertEquals("Size should match header", blob1.getContentSize(),
                Long.parseLong(blob1.getHeaders().get(BlobHeader.CONTENTSIZE.headerKey())));

        // adding custom headers must not affect system headers
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("category", "sports");
        headers.put("score", "1");
        blob1.updateHeaders(headers, false);

        assertNotNull("Headers should not be null", blob1.getHeaders());
        assertNotNull("UUID should not be null", blob1.getUUID());
        assertEquals("UUID should match header", blob1.getUUID(), blob1.getHeaders().get(BlobHeader.UUID.headerKey()));
        assertNotNull("Accessed should not be null", blob1.getLastAccessed());
        assertEquals("Accessed should match header", blob1.getLastAccessed(),
                new Date(Long.parseLong(blob1.getHeaders().get(BlobHeader.LASTACCESSED.headerKey()))));
        assertNotNull("Created should not be null", blob1.getCreated());
        assertEquals("Created should match header", blob1.getCreated(),
                new Date(Long.parseLong(blob1.getHeaders().get(BlobHeader.CREATED.headerKey()))));
        assertNotNull("Modified should not be null", blob1.getLastModified());
        assertEquals("Modified should match header", blob1.getLastModified(),
                new Date(Long.parseLong(blob1.getHeaders().get(BlobHeader.LASTMODIFIED.headerKey()))));
        assertNotNull("MD5 should not be null", blob1.getContentMD5());
        assertEquals("MD5 should match header", blob1.getContentMD5(),
                blob1.getHeaders().get(BlobHeader.CONTENTMD5.headerKey()));
        assertNotNull("ContentType should not be null", blob1.getContentType());
        assertEquals("ContentType should match header", blob1.getContentType(),
                blob1.getHeaders().get(BlobHeader.CONTENTTYPE.headerKey()));
        assertFalse("Size should not be -1", blob1.getContentSize() == -1);
        assertEquals("Size should match header", blob1.getContentSize(),
                Long.parseLong(blob1.getHeaders().get(BlobHeader.CONTENTSIZE.headerKey())));

        assertEquals("Category should be sports", blob1.getHeaders().get("category"), "sports");
        assertEquals("Score should be 1", blob1.getHeaders().get("score"), "1");

        // test retainExisting=true
        headers = new HashMap<String, String>();
        headers.put("color", "blue");
        blob1.updateHeaders(headers, true);

        assertEquals("Category should be sports", blob1.getHeaders().get("category"), "sports");
        assertEquals("Score should be 1", blob1.getHeaders().get("score"), "1");
        assertEquals("Color should be blue", blob1.getHeaders().get("color"), "blue");

        // test retainExisting=false
        headers = new HashMap<String, String>();
        headers.put("color", "red");
        blob1.updateHeaders(headers, false);

        assertEquals("Color should be red", blob1.getHeaders().get("color"), "red");
        assertNull(blob1.getHeaders().get("category"));
        assertNull(blob1.getHeaders().get("score"));

        System.err.println(blob1.getHeaders());

    }

    public void testStoreFind() throws Exception {

        BlobStore store = getBlobStore();
        Blob blob1 = store.addBlob(getRandomInputStream(), "application/vnd.amdatu.x");
        Blob blob2 = store.addBlob(getRandomInputStream(), "application/vnd.amdatu.x");
        Blob blob3 = store.addBlob(getRandomInputStream(), "application/vnd.amdatu.y");
        Blob blob4 = store.addBlob(getRandomInputStream(), "application/vnd.amdatu.y");

        List<Blob> result = store.findBlobs("(" + BlobHeader.CONTENTTYPE.headerKey() + "=application/vnd.amdatu.x)");
        assertEquals("Results should be 2", 2, result.size());
        assertTrue(result.contains(blob1));
        assertTrue(result.contains(blob2));

        Map<String, String> headers = new HashMap<String, String>();
        headers.put("category", "sports");
        headers.put("score", "1");

        blob1.updateHeaders(headers, false);

        headers.put("score", "2");
        blob2.updateHeaders(headers, false);

        headers.put("category", "fun");
        headers.put("score", "1");
        blob3.updateHeaders(headers, false);

        headers.put("score", "2");
        blob4.updateHeaders(headers, false);

        result = store.findBlobs("(|(score>=2)(category=sports))");
        assertEquals("Results should be 3", 3, result.size());
        assertTrue(result.contains(blob1));
        assertTrue(result.contains(blob2));
        assertTrue(result.contains(blob4));
    }

    public void testStoreBlobRetention() throws Exception {

        BlobStore store = getBlobStore();
        Blob blob1 = store.addBlob(getRandomInputStream(), "application/vnd.amdatu.x");

        for (int i = 0; i < 10000; i++) {
            store.addBlob(getRandomInputStream(), "application/vnd.amdatu.x");
        }

        Blob blob2 = store.getBlob(blob1.getUUID());
        assertTrue(blob1 == blob2);
    }

}
