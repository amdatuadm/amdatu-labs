package org.amdatu.blobstore.core;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.UUID;

import junit.framework.TestCase;

public class DigestTest extends TestCase {

    public void testDigest() throws Exception {

        byte[] bytes = UUID.randomUUID().toString().getBytes();
        System.out.println(bytes.length);
        
        MessageDigest md1 = MessageDigest.getInstance("MD5");
        md1.update(bytes);
        System.err.println(new BigInteger(1, md1.digest()).toString(16));

        MessageDigest md2 = MessageDigest.getInstance("MD5");
        md2.update(bytes);
        System.err.println(new BigInteger(1, md2.digest()).toString(16));

        
        MessageDigest md = MessageDigest.getInstance("MD5");
        DigestInputStream is = new DigestInputStream(new ByteArrayInputStream(bytes), md);

        int b = 0;
        int i = 0;
        while ((b = is.read()) != -1) {
            i++;
        }
        System.out.println(i);

        byte[] md5 = md.digest();
        System.err.println(new BigInteger(1, md5).toString(16));
    }
}
