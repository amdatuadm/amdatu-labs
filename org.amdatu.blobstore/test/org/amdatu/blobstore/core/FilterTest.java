package org.amdatu.blobstore.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;

public class FilterTest extends TestCase {

    
    public void testFilter() throws Exception {
        
        Filter filter1 = FrameworkUtil.createFilter("(category=sports)");
        Filter filter2 = FrameworkUtil.createFilter("(score<=4)");
        
        Map<String, Object> headers1 = new HashMap<String, Object>();
        headers1.put("category", "sports");
        headers1.put("score", 1);

        assertTrue(filter1.matches(headers1));
        assertTrue(filter2.matches(headers1));
        
        Map<String, Object> headers2 = new HashMap<String, Object>();
        List<String> values = new ArrayList<String>();
        values.add("sports");
        headers2.put("category", values);
        headers1.put("score", 5);

        assertTrue(filter1.matches(headers2));
        assertFalse(filter2.matches(headers2));

    }
}
